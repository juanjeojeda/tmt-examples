#!/bin/bash

set -e

source ./lib.sh

# Tests

binary_exists /bin/ls

binary_exists /bin/dnf

binary_exists /bin/ps

check_fails
