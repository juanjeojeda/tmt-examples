#!/bin/bash

set -e

declare -i FAILS

binary_exists (){
  BIN_FILE="${1}"

  echo -en "Checking ${BIN_FILE}...\t"
  if [[ -x "${BIN_FILE}" ]]; then
    echo "PASS"
  else
    echo "FAIL"
    (( FAILS += 1 ))
  fi
}

check_fails (){
  if (( FAILS > 0 )); then
    echo "Total of FAILS: ${FAILS}"
    exit 1
  fi
  echo "All tests passed"
  exit 0
}

